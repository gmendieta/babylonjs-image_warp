var Gui = function (app) {
    this.m_app = app;
    this.m_engine = app.m_engine;
    this.m_scene = app.m_scene;

    this.init();
};

Gui.prototype.init = function() {
    this.m_gui = BABYLON.GUI.AdvancedDynamicTexture.CreateFullscreenUI('ui');

    var stats_panel = new BABYLON.GUI.StackPanel();
    stats_panel.width = "220px";
    stats_panel.fontSize = "14px";
    stats_panel.horizontalAlignment = BABYLON.GUI.Control.HORIZONTAL_ALIGNMENT_LEFT;
    stats_panel.verticalAlignment = BABYLON.GUI.Control.VERTICAL_ALIGNMENT_TOP;
    this.m_gui.addControl(stats_panel);

    this.m_fps_txt = new BABYLON.GUI.TextBlock();
    this.m_fps_txt.text = "";
    this.m_fps_txt.height = "40px";
    this.m_fps_txt.color = "white";
    this.m_fps_txt.paddingTop = "10px";
    stats_panel.addControl(this.m_fps_txt);

    var options_panel = new BABYLON.GUI.StackPanel();
    options_panel.width = "220px";
    options_panel.fontSize = "14px";
    options_panel.horizontalAlignment = BABYLON.GUI.Control.HORIZONTAL_ALIGNMENT_RIGHT;
    options_panel.verticalAlignment = BABYLON.GUI.Control.VERTICAL_ALIGNMENT_TOP;
    this.m_gui.addControl(options_panel);

    var header = new BABYLON.GUI.TextBlock();
    header.text = "Spline Count:";
    header.height = "40px";
    header.color = "white";
    header.textHorizontalAlignment = BABYLON.GUI.Control.HORIZONTAL_ALIGNMENT_LEFT;
    header.paddingTop = "10px";
    options_panel.addControl(header); 

    var slider = new BABYLON.GUI.Slider();
    slider.minimum = 4;
    slider.maximum = 16;
    slider.value = this.m_scene.m_num_beziers;
    slider.height = "20px";
    slider.width = "200px";
    var _this = this;
    slider.onValueChangedObservable.add(function(value) {
        // Without the int conversion it will fail!!
        _this.m_scene.setNumCurves(Math.floor(value));
    });
    options_panel.addControl(slider);

    // Debug Button
    var button = BABYLON.GUI.Button.CreateSimpleButton("debug_btn", "Show Debug");
    button.height = "50px";
    header.color = "white";
    button.onPointerClickObservable.add(function() {
        _this.m_scene.flipShowDebug();
    });
    options_panel.addControl(button);

};

Gui.prototype.update = function() 
{
    this.m_fps_txt.text = this.m_engine.getFps().toFixed() + " FPS";
}