window.addEventListener('DOMContentLoaded', function()
{
    new App('appCanvas');
}, false);

var App = function(canvas_id) {
    var canvas = document.getElementById(canvas_id);
    this.m_engine = new BABYLON.Engine(canvas, true);
    this.m_scene = new Scene(this);
    //this.m_gui = new Gui(this);
    // All 3d models
    this.initApp();
    
    var _this = this;
    this.m_engine.runRenderLoop(function() {
        //_this.m_gui.update();
        _this.m_scene.render();
    });
    // Watch for browser/canvas resize events
    window.addEventListener("resize", function () {
        _this.m_engine.resize();
        _this.m_scene.resize();
});
};

App.prototype.initApp = function() {
    //this.m_scene.debugLayer.show();
};