var c_debug = true;
var c_num_curves = 6; // Number of beziers for each row and col
var c_control_size = 3; // Diameter of the Control
var c_curve_subdiv = 5; // Segments between each curve points
var c_use_control_coords = true;

var c_control_coords = [
    [-0.136312,0.220146],
    [-0.115184,0.390236],
    [-0.085970,0.573611],
    [-0.052022,0.771887],
    [-0.022230,0.985205],
    [0.003275,1.220209],
    [0.099880,0.108963],
    [0.126648,0.262374],
    [0.153345,0.439777],
    [0.174019,0.631696],
    [0.191901,0.844644],
    [0.201634,1.078206],
    [0.374977,0.051255],
    [0.378799,0.194421],
    [0.387301,0.364681],
    [0.393330,0.552904],
    [0.398835,0.766292],
    [0.402078,1.001474],
    [0.641670,0.055231],
    [0.628264,0.194885],
    [0.618296,0.364568],
    [0.609982,0.553181],
    [0.603052,0.764457],
    [0.599972,1.009208],
    [0.905903,0.119488],
    [0.873094,0.265019],
    [0.847163,0.438800],
    [0.824111,0.626995],
    [0.805983,0.836886],
    [0.800105,1.061260],
    [1.184148,0.230937],
    [1.124222,0.391612],
    [1.075971,0.569320],
    [1.039222,0.759840],
    [1.008467,0.964489],
    [0.998766,1.146076]
];