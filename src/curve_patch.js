/**
 * This class implements a Curve Patch. That is a Mesh that is generated
 * by a set of curves. At the moment its not deriving the points to generate the Mesh.
 * The method getPoints will be called, so the number of subdivisions which are used
 * when defining the curves will be used.
 */
var CurvePatch = function(scene)
{
    this.m_scene = scene
    this.m_mesh = null;
};

/**
 * This method updates the vertices of the Mesh.
 * width: number of curves to use horizontally
 * height: number of curves to use vertically
 * subdivisions: Number of quads between each curve
 */
CurvePatch.prototype.updatePatch = function(row_curves, col_curves, subdivisions)
{
    // Array of Arrays representing Row and Column curves already subdivided
    // The total number of points for each row or coll will be the formula
    // num_row_points = row_curves.length + (subdivisions - 1) * (row_curves.length - 1);
    // num_col_points = col_curves.length + (subdivisions - 1) * (col_curves.length - 1);
    var row_points = [];
    for (var col=0; col<row_curves.length; ++col) {
        row_points.push(row_curves[col].getPoints());
    }
    var col_points = [];
    for (var col=0; col<col_curves.length; ++col) {
        col_points.push(col_curves[col].getPoints());
    }
    positions = [];
    for (var row=1; row<row_curves.length; ++row)
    {
        for (var row_div=0; row_div<subdivisions; ++row_div)
        {
            var row_point_idx = ((row - 1) * subdivisions) + row_div;
            // Row Lerp ratio
            var row_ratio = row_div / subdivisions;
            // Calculate the height for this subdivision
            for (var col=1; col<col_curves.length; ++col)
            {
                for (var col_div=0; col_div<subdivisions; ++col_div)
                {
                    // Col Lerp ratio
                    var col_ratio = col_div / subdivisions;
                    var col_point_idx = ((col - 1) * subdivisions) + col_div;
                    var x1 = col_points[col-1][row_point_idx].x;
                    var x2 = col_points[col][row_point_idx].x;
                    var y1 = row_points[row-1][col_point_idx].y;
                    var y2 = row_points[row][col_point_idx].y;
                    positions.push(
                        ((1-col_ratio)*x1) + (col_ratio*x2),
                        ((1-row_ratio)*y1) + (row_ratio*y2),
                        0
                    );
                }
            }
            // Add the last column row point
            var pt = col_points[col_curves.length-1][row_point_idx];
            positions.push(
                pt.x,
                pt.y,
                0
            );
        }    
    }
    // Add the last row points
    for (col=0; col<row_points[row_points.length-1].length; ++col)
    {
        var pt = row_points[row_points.length-1][col];
        positions.push(
            pt.x,
            pt.y,
            0
        );
    }

    num_row_points = row_curves.length + (subdivisions - 1) * (row_curves.length - 1);
    num_col_points = col_curves.length + (subdivisions - 1) * (col_curves.length - 1);
    row_uv_step = 1.0 / (num_row_points - 1);
    col_uv_step = 1.0 / (num_col_points - 1);

    // Generate Indexes, UVs and Normals
    uvs = []
    indices = []
    normals = []
    for (var row=0; row<num_row_points; ++row)
    {
        for (var col=0; col<num_col_points; ++col)
        {
            // UV Coordinates
            uvs.push(
                col * col_uv_step,
                row * row_uv_step
            );
            // Normals
            normals.push(
                0, 0, -1
            );
        }
    }
    for (var row=0; row<num_row_points-1; ++row)
    {
        for (var col=0; col<num_col_points-1; ++col)
        {
            // First triangle indices
            indices.push(
                (row*num_row_points)+col,
                (row*num_row_points)+col+1,
                ((row+1)*num_row_points)+col+1
            );
            // Second triangle indices
            indices.push(
                (row*num_row_points)+col,
                ((row+1)*num_row_points)+col+1,
                ((row+1)*num_row_points)+col
            );
        }
    }
    // The first time we generate the image has to be this way
    if (!this.m_mesh)
    {
        vertex_data = new BABYLON.VertexData();
        vertex_data.positions = positions;
        vertex_data.indices = indices;
        vertex_data.uvs = uvs;
        vertex_data.normals = normals;
        this.m_mesh = new BABYLON.Mesh("patch", this.m_scene);
        this.m_mesh.material = this.m_scene.getMaterialByName("patch");
        vertex_data.applyToMesh(this.m_mesh, true);
    }
    else
    {
        this.m_mesh.updateVerticesData(BABYLON.VertexBuffer.PositionKind, positions);
    }
};