
var Scene = function (app) {
    this.m_app = app;
    this.m_engine = app.m_engine;
    this.m_scene = new BABYLON.Scene(this.m_engine);
    this.m_camera = null;
    this.m_width = null;
    this.m_height = null;
    this.m_controls = []; // Bezier Controls
    this.m_row_curves = []; // Babylon Bezier Curves
    this.m_col_curves = []; // Babylon Bezier Curves
    this.m_lines = []; // Bezier Paths for drawing
    this.m_curve_patch = null;
    this.m_num_curves = c_num_curves;
    this.m_control_size = c_control_size;
    this.m_curve_subdiv = c_curve_subdiv
    this.m_show_debug = c_debug;
    this.init();

    // GUI will modify m_new_num_beziers and after draw it will be check
    this.m_next_num_curves = this.m_num_curves;
    // GUI will use m_curr_control to drag control meshes
    this.m_curr_control = null;
};

Scene.prototype.init = function() {
    var hemi = new BABYLON.HemisphericLight("hemi", new BABYLON.Vector3(0, 0, -10), this.m_scene);
    hemi.intensity = 1;

    this.calculateSize();
    this.initCamera();
    this.initMaterials();
    this.initCurveControls();
    this.initEventHandling();
    this.initCurvePatch();

    if (c_debug)
        this.m_scene.debugLayer.show();
};

Scene.prototype.render = function() {
    this.updateCurves();
    this.m_curve_patch.updatePatch(this.m_row_curves, this.m_col_curves, this.m_curve_subdiv);
    this.m_scene.render();
    // GUI will modify m_new_num_beziers
    if (this.m_num_curves != this.m_next_num_curves) {
        this.m_num_curves = this.m_next_num_curves;
        this.initCurveControls();
    }
};

Scene.prototype.resize = function() {
    this.updateCameraOrthoSize(this.m_camera);
};

Scene.prototype.calculateSize = function() {
    this.m_height = 100;
    var ratio = window.innerWidth / window.innerHeight;
    this.m_width = this.m_height * ratio;
    console.info("The size is %d x %d", this.m_width, this.m_height);
};

Scene.prototype.initCamera = function() {
    this.m_camera = new BABYLON.FreeCamera("main", new BABYLON.Vector3(0, 0, -10), this.m_scene);
    this.m_camera.mode = BABYLON.Camera.ORTOGRAPHIC_CAMERA;
    this.m_camera.setTarget(BABYLON.Vector3.Zero());
    this.updateCameraOrthoSize(this.m_camera);
    
    //this.m_camera.attachControl(this.m_engine.getRenderingCanvas());
};

Scene.prototype.updateCameraOrthoSize = function(camera) {
    this.calculateSize();
    camera.orthoLeft = -this.m_width / 2;
    camera.orthoRight = this.m_width / 2;
    camera.orthoTop = this.m_height / 2;
    camera.orthoBottom = -this.m_height / 2;
    // This way all the coordinates to generate the scene are positive
    camera.position = new BABYLON.Vector3(this.m_width / 2, this.m_height / 2, -10);
    camera.getProjectionMatrix(true);
};

Scene.prototype.initMaterials = function() {
    var mat = new BABYLON.StandardMaterial("control", this.m_scene);
    mat.diffuseColor = BABYLON.Color3.Red();
    mat.specularColor = BABYLON.Color3.Black();

    // Curve Patch material
    mat = new BABYLON.StandardMaterial("patch", this.m_scene);
    mat.specularColor = BABYLON.Color3.Black();
    mat.diffuseTexture = new BABYLON.Texture("tex/msfsx.jpg");
}

Scene.prototype.flipShowDebug = function()
{
    this.m_show_debug = !this.m_show_debug;
    if (this.m_show_debug)
        this.m_scene.debugLayer.show();
    else
        this.m_scene.debugLayer.hide();
}

Scene.prototype.setNumCurves = function(num_curves)
{
    this.m_next_num_curves = num_curves;
}

Scene.prototype.initCurveControls = function()
{
    var size = this.m_width;
    if (this.m_height < this.m_width) {
        size = this.m_height;
    }
    size *= 0.75;

    var v_padding = (this.m_height - size) / 2;
    var h_padding = (this.m_width - size) / 2;
    var step = size / (this.m_num_curves - 1);
    // Delete all the controls before redo them
    for (var it=0; it<this.m_controls.length; ++it) {
        this.m_controls[it].dispose();
    }
    this.m_controls = [];
    for (var row=0; row < this.m_num_curves; ++row) {
        for (var col=0; col < this.m_num_curves; ++col) {
            var sphere = BABYLON.MeshBuilder.CreateSphere("control", {diameter: this.m_control_size}, this.m_scene);
            if (c_use_control_coords)
            {
                // There is no Check here
                normalized_coords = c_control_coords[col*this.m_num_curves+row];
                var cam_coords = this.normalized2Camera(this, normalized_coords[0], normalized_coords[1]);
                sphere.position = new BABYLON.Vector3(cam_coords.x, cam_coords.y, 0);
            }
            else
            {
                sphere.position = new BABYLON.Vector3(h_padding + step * col, v_padding + step * row, 0);
            }
            sphere.material = this.m_scene.getMaterialByName("control");
            this.m_controls.push(sphere);
        }
    }
};

Scene.prototype.initCurvePatch = function() 
{
    this.m_curve_patch = new CurvePatch(this.m_scene);
}

Scene.prototype.getRowPositions = function(row) {
    var positions = [];
    for (var col=0; col<this.m_num_curves; ++col) {
        positions.push(this.m_controls[(row * this.m_num_curves) + col].position);
    }
    return positions;
}

Scene.prototype.getColPositions = function(col) {
    var positions = [];
    for (var row=0; row<this.m_num_curves; ++row) {
        positions.push(this.m_controls[row * this.m_num_curves + col].position);
    }
    return positions;
}

Scene.prototype.updateCurves = function() {
    for (var row=0; row<this.m_row_curves.length; ++row) {
        delete this.m_row_curves[row];
    }
    this.m_row_curves = [];
    for (var col=0; col<this.m_col_curves.length; ++col) {
        delete this.m_col_curves[col];
    }
    this.m_col_curves = [];

    // Row Beziers
    for (var row=0; row<this.m_num_curves; ++row) {
        this.m_row_curves.push(BABYLON.Curve3.CreateCatmullRomSpline(
            this.getRowPositions(row),
            this.m_curve_subdiv,
            false
        ));
    }
    // Col Beziers
    for (var col=0; col<this.m_num_curves; ++col) {
        this.m_col_curves.push(BABYLON.Curve3.CreateCatmullRomSpline(
            this.getColPositions(col),
            this.m_curve_subdiv,
            false
        ));
    }

    for (var row=0; row<this.m_lines.length; ++row) {
        this.m_lines[row].dispose();
    }
    this.m_lines = [];
    for (var row=0; row<this.m_row_curves.length; ++row) {
        var line = BABYLON.Mesh.CreateLines("curve_line", this.m_row_curves[row].getPoints(), this.m_scene);
        line.color = BABYLON.Color3.Green();
        this.m_lines.push(line);
    }
    for (var col=0; col<this.m_col_curves.length; ++col) {
        var line = BABYLON.Mesh.CreateLines("curve_line", this.m_col_curves[col].getPoints(), this.m_scene);
        line.color = BABYLON.Color3.Green();
        this.m_lines.push(line);
    }
}

Scene.prototype.initEventHandling = function() {

    var _this = this;
    var onPointerDown = function(event) {
        var cam_coords = _this.pixel2Camera(_this, _this.m_scene.pointerX, _this.m_scene.pointerY);
        var pick_info = _this.m_scene.pick(_this.m_scene.pointerX, _this.m_scene.pointerY);
        if (pick_info.hit == false)
            return;
        if (pick_info.pickedMesh && pick_info.pickedMesh.name == 'control')
        {
            _this.m_curr_control = pick_info.pickedMesh;
        }
    };
    
    var onPointerUp = function(event) {
        _this.m_curr_control = null;
    };
    
    var onPointerMove = function(event) {
        if (!_this.m_curr_control) {
            return;
        }
        var cam_coords = _this.pixel2Camera(_this, _this.m_scene.pointerX, _this.m_scene.pointerY);
        var position = new BABYLON.Vector3(cam_coords.x, cam_coords.y, 0);
        _this.m_curr_control.position = position;
    };

    var canvas = this.m_engine.getRenderingCanvas();
    canvas.addEventListener("pointerdown", onPointerDown, function (mesh) { return mesh.name == control; });
    canvas.addEventListener("pointerup", onPointerUp, false);
    canvas.addEventListener("pointermove", onPointerMove, false);

    this.m_scene.dispose = function() {
        canvas.removeEventListener("pointerdown", onPointerDown);
        canvas.removeEventListener("pointerup", onPointerUp);
        canvas.removeEventListener("pointermove", onPointerMove);
    }
};

// This could be the Remap function
Scene.prototype.pixel2Camera = function(scene, x, y) {
    var cam_x = scene.m_scene.pointerX / window.innerWidth * scene.m_width;
    var cam_y = scene.m_scene.pointerY / window.innerHeight * scene.m_height;
    //console.info('Camera coordinates %d x %d', cam_x, cam_y);
    return {
        x: cam_x,
        y: scene.m_height - cam_y // Vertical pixels starts at the bottom
    };
}

Scene.prototype.normalized2Camera = function(scene, x, y) {
    var cam_x = scene.m_width * x;
    var cam_y = scene.m_height * y;
    return {
        x: cam_x,
        y: cam_y
    };
}

Scene.prototype.reset = function() {
    ;
};

